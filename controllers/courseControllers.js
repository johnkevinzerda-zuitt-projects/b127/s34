const Course = require('../models/Course');



//Creation of Course
/*
Steps:
1. Create a conditional statement that will check if the user is an admin.
2. Create a new course object using the mongooose model and the information from the request body and the ID from the header
3. Save the new Course to the database

*/


module.exports.addCourse = (data) => {
	//User id admin
	if (data.isAdmin){

		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) =>{
			//Course creation failed
			if (error) {
				return false;
			}else {
				return true;
			}
		})

	}else {
		//User is not an admin
		return false;
	}
}


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


//Retrieve all ACTIVE courses
module.exports.getAllActive = () => {
	return find({isActive: true}).then(result => {
		return result;
	})	
}


//Retrieve SPECIFIC course
module.exports.getSpecficCourse = (reqParams) => {
	return findById(reqParams.courseId).then(result=>{
		return result;
	})
}


//Update a Course

module.exports.updateCourse = (reqParams,data) => {

	if (data.isAdmin) {
		return Course.findByIdAndUpdate(reqParams.courseId,
			{

				price:data.newUpdate.price,
		 		name:data.newUpdate.name,
		 		description:data.newUpdate.description

			}).then(result => {
				return result;
			})
	}
}
