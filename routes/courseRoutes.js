const express = require("express");
const router = express.Router();
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

//Route for creating a course
//localhost:4000/courses/
router.post('/', auth.verify, (req,res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));

})

router.get('/all',(req,res) => {
	courseController.getAllCourses().then(result=> res.send(result));
})


//Retrive all TRUE courses
router.get('/',(req,res)=>{
	courseController.getAllActive().then(result => res.send(result));
})


//Retrieve SPECIFIC course
router.get('/:courseId',(req,res) => {
	courseController.getSpecificCourse(req.params).then(result => res.send(result));
})


router.put('/:courseId', auth.verify, (req,res) => {

	const data = {
		newUpdate: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	courseController.updateCourse(req.params, data).then(result => res.send(result))
})

module.exports = router;